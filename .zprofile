export PATH="$PATH:$HOME/.bin:/var/lib/snapd/snap/bin" # Add local helper scripts to path
export MOZ_ENABLE_WAYLAND=1
if [[ -z "${DISPLAY}" ]] && [[ "${XDG_VTNR}" -eq 2 || "${XDG_VTNR}" -eq 1 ]]; then
  export $(gnome-keyring-daemon --start)
  export XDG_CURRENT_DESKTOP=sway
  export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/gcr/ssh
  # exec dbus-run-session sway
  exec sway
fi
