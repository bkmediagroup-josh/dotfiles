#!/bin/bash

i3-msg "workspace 1:keepass; append_layout ~/.config/i3/workspace-1.json"
i3-msg "workspace 2:tasks; append_layout ~/.config/i3/workspace-2.json"
i3-msg "workspace 3:chat; append_layout ~/.config/i3/workspace-3.json"
i3-msg "workspace 4:email; append_layout ~/.config/i3/workspace-4.json"
i3-msg "workspace 5:web; append_layout ~/.config/i3/workspace-5.json"
i3-msg "workspace 6:work 1; append_layout ~/.config/i3/workspace-6.json"

keepassx &
# workamajig 
chromium --app="https://app14.workamajig.com/platinum/" --enable-extensions &
sleep 1
# calendar
chromium --app="https://calendar.google.com/calendar/b/1/r?tab=mc" --enable-extensions &
# messages
sleep 1
chromium --app="https://messages.android.com/" --enable-extensions &
# gmail_work
sleep 1
chromium --app="https://mail.google.com/mail/u/1/#inbox" --enable-extensions &
# gmail_oushinhogin
sleep 1
chromium --app="https://mail.google.com/mail/u/0/#inbox" --enable-extensions &
# protonmail
sleep 1
chromium --app="https://mail.protonmail.com/inbox" --enable-extensions &
qutebrowser &

urxvt &
slack -u &
/usr/bin/dropbox &
/usr/bin/owncloud &
