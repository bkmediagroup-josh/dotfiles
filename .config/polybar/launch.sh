#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down

export MONITOR=$(polybar -m|tail -1|head -1|sed -e 's/:.*$//g')
export MONITOR2=$(polybar -m|tail -2|head -1|sed -e 's/:.*$//g')

echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log > /dev/null

# Launch bar1 and bar2
polybar primary 2>&1 | tee -a /tmp/polybar1.log > /dev/null & disown
if [[ $MONITOR != $MONITOR2 ]]
then
  polybar secondary 2>&1 | tee -a /tmp/polybar2.log > /dev/null & disown
  echo "$MONITOR"
  i3-msg "[workspace=\"1\"] move workspace to output $MONITOR"
  i3-msg "[workspace=\"2\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"3\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"4\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"5\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"6\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"7\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"8\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"9\"] move workspace to output $MONITOR2"
  i3-msg "[workspace=\"10\"] move workspace to output $MONITOR2"
fi

echo "Bars launched..."
