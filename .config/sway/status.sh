# The Sway configuration file in ~/.config/sway/config calls this script.
# You should see changes to the status bar after saving this script.
# If not, do "killall swaybar" and $mod+Shift+c to reload the configuration.

# Produces "21 days", for example
uptime_formatted=$(uptime | cut -d ',' -f1  | cut -d ' ' -f4,5)

# The abbreviated weekday (e.g., "Sat"), followed by the ISO-formatted date
# like 2018-10-06 and the time (e.g., 14:01)
date_formatted=$(date "+%a %F %I:%M %p")

# Get the Linux version but remove the "-1-ARCH" part
linux_version=$(uname -r | cut -d '-' -f1)

# Returns the battery status: "Full", "Discharging", or "Charging".
battery_status=$(cat /sys/class/power_supply/BAT0/status | cut -c1)
battery_percentage=$(cat /sys/class/power_supply/BAT0/capacity)
performance_mode=$(cat /sys/firmware/acpi/platform_profile)

volume=$(pactl get-sink-volume 0 | head -n 1 | cut -d ',' -f1 | cut -d '/' -f2 | xargs)

# Emojis and characters for the status bar
# 💎 💻 💡 🔌 ⚡ 📁 \|
echo "$volume | $performance_mode $battery_percentage% $battery_status | $date_formatted"
