#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# alias startx="startx -- -dpi 125 -ardelay 200 -arinterval 50"
# alias startx="startx -- -ardelay 200 -arinterval 50"
alias startlowx="startx -- -dpi 90 -ardelay 200 -arinterval 50"
alias tmux="TERM=screen-256color tmux"

export ALSA_CARD=PCH
export EDITOR=vim

set -o vi

source /usr/share/git/completion/git-prompt.sh

#PS1='[\u@\h \W]\$ '
PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

export PATH="$PATH:$HOME/.bin" # Add local helper scripts to path
export PATH="$PATH:/usr/local/heroku/bin"

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/base16-default.dark.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

# if [[ -z "$TMUX" && "$TERM" == "xterm" ]] ;then # thix checks if we are already in a tmux AND if we are in a gui terminal
#     ID="`tmux ls | grep -vm1 attached | cut -d: -f1`" # get the id of a deattached session
#     if [[ -z "$ID" ]] ;then # if not available create a new one
#         tmux new-session
#     else
#         tmux attach-session -t "$ID" # if available attach to it
#     fi
# fi

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -f ~/.fzf.colors ] && source ~/.fzf.colors
