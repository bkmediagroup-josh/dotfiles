execute pathogen#infect()

syntax on
filetype plugin indent on
set number
set ignorecase
set expandtab
set tabstop=2
set shiftwidth=2
set nowrap
set hls

set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

map <Leader>gs :Git<CR>
map <Leader>c :!wl-copy < %<CR>
map <Leader>d :!./deploy.sh<CR>

map ]q :cn<CR>
map [q :cp<CR>
map ]b :bnext<CR>
map [b :bprev<CR>

cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))
cnoreabbrev <expr> Wq ((getcmdtype() is# ':' && getcmdline() is# 'Wq')?('wq'):('Wq'))

let g:ctrlp_working_path_mode = 'a'

"au BufNewFile,BufRead *.php set ft=phtml

set wildmenu
set wildmode=longest,list,full
set wildignore=.git,*.swp,*/tmp/*

nnoremap <Leader>q :Bdelete<CR>

augroup BgHighlight
  autocmd!
  autocmd WinEnter * hi StatusLine ctermfg=red
  autocmd WinLeave * hi StatusLine ctermfg=black
augroup END

" autocmd BufEnter *.php setlocal ft=phtml
autocmd BufEnter *.inky setlocal ft=haml
au BufRead,BufNewFile qutebrowser-editor*,*.txt,*.tex set wrap linebreak nolist textwidth=0 wrapmargin=0
au VimResized * wincmd  =

autocmd BufLeave * if &modified | update | endif

let g:indentLine_setConceal = 0

let base16colorspace=256
colorscheme base16-gruvbox-dark-hard

let g:vue_pre_processors = ['scss']
